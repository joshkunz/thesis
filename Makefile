.PHONY: clean

doc = thesis
sources = $(doc).tex $(shell find ./content -name '*.tex')
auxfiles = $(patsubst %.tex,%.aux,$(sources))

$(doc).pdf: $(sources) mythesis.sty
	pdflatex $(doc).tex
	bibtex $(doc)
	pdflatex $(doc).tex
	pdflatex $(doc).tex
	pdflatex $(doc).tex

clean:
	-rm -f $(addsuffix .new,$(sources))
	-rm -f $(doc).pdf $(doc).log $(doc).aux $(doc).bbl $(doc).lot $(doc).lof $(doc).out $(doc).blg $(doc).toc
	-rm -f $(auxfiles)
