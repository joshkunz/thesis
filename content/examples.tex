\chapter{Example Protocols}
\label{sec:examples}

In this chapter, we detail an example isolation protocol that can be
constructed using the \name{} model: Application as a Service.
The Application as a Service protocol is designed to address a common collaboration
problem in modern clouds. Today, there is no way for a service provider and customer
who are mutually distrusting to cooperate. Either the customer must trust
the service provider with its data, or the service provider must trust the customer
with its application or data. Sometimes this is not even feasible. \name{} addresses
this problem by using the Two-Way Isolation building block as described in
Section~\ref{sec:model:building-blocks:two-way}.

\section{Application as a Service}
\label{sec:examples:aaas}

Application as a Service is a protocol between two parties, a customer and
a provider. We'll denote them as $C$ and $P$. The goal of the protocol is to allow
$C$ to use a service provided by $P$ without $C$ and $P$ trusting each other.
Specifically, we will create a cluster of nodes $P'$. Intuitively, this will be
an isolated cluster of provider ($P$) controlled nodes that can communicate with
$C$ but cannot communicate with $P$. Additionally, since we don't want $C$ to be
able to connect \textit{arbitraily} to $P'$, the connectivity between $P'$ and
$C$ will be created by $P$. So, assuming that each party is rational,
self-interested, and distrusting, they will develop the minimal amount of 
connectivity required to realize the service provider $P$ is trying to provide.

For simplicity of explanation, we will assume that a few capabilities exist. We
assume that both $C$ and $P$ own a capability to some rendezvous point. We call
this the service rendezvous point. It is the communication channel over which
the protocol is performed. Further, we assume that $C$ has capabilities to
a set of Node objects. We'll call this set of capabilities $C_{nodes}$.

The pseudocode for this protocol can be found in Figures~\ref{lst:aaas-customer} and
\ref{lst:aaas-provider}. It consists of two major
pieces, one for the customer to execute, and one for the provider to execute.
The customer code can be found in Figures~\ref{lst:aaas-customer}, and the
provider code can be found in Figures~\ref{lst:aaas-provider}. The customer initiates
the protocol by invoking \lstinline{request_service} with the service rp as an
argument, and another rp, the node rp, on which the customer has already sent
capabilities for the nodes that will become the $P'$ cluster. Before sending
the capabilities to these nodes, $C$ creates a membrane, and wraps them with the
membrane. As described in Section~\ref{sec:model:para:membranes}, this allows $C$ to later remove all
capabilities $P$ has to these objects. Since rendezvous
points gracefully block until there are capabilities available, the provider has
already called \lstinline{receive_service_request} earlier with the shared
service rp. It additionally takes a rendezvous point on which the nodes for the
request will be stored. Once \lstinline{receive_service_request} begins receiving
node capabilities, it resets them and then sends them on the supplied node rp.
The reset is important. It guarantees to $P$ that the nodes also no longer have
connectivity to any node in $C$. Therefore, $P$ now knows that it can safely
transfer its proprietary service logic and data onto the nodes without fear
that $C$ will be able to exfiltrate it. After the nodes have been received and
reset, $P$ configures the cluster with the service it will be providing to $C$.
As part of this configuration, $P$ creates a rendezvous point on the node
that will serve as a frontend for the service. This rendezvous point will be
how all connectivity between $C$ and $P'$ is created. To finalize the service
setup, the created rp is sent back to $C$ over the service rp.

Finally, the customer receives this resulting rp, and then clears the membrane.
At this point, the protocol is finished, and the security guarantees are established.
Due to the membrane, no node in $P$ can communicate with $P'$. Only $C$ holds
a capability to the rendezvous point in $P'$. However, since the nodes were
reset, $C$ holds no capabilities to any objects (for example nodes) in $P'$
except for the rendezvous point it just received. It can only obtain capabilities
to the nodes in $P'$ according to the logic in $P'$, which was defined by $P$, who
has established a secure frontend for the service it is providing in $P'$.
It is also important to note that $P'$ is in fact composed of the nodes that $C$
sent to $P$, as $P$ could potentially try to de-isolate $C$'s data by creating
a cluster from nodes that did not pass through the membrane (and therefore are
still connected to $P$ after the membrane is cleared). However, this is not possible.
Since $P$ sent the result rp back through the membrane, if it was not already
wrapped by the membrane, it would have been destroyed when the membrane was
cleared and the protocol would have failed ($C$ would not have been able to use the
rp). This dual-sided nature of membranes is critical to the operation of this
protocol. It guarantees not only that the original nodes are isolated, but that
they are in fact the same nodes $C$ originally gave to $P$.
At this point, $C$ can be sure that $P'$ is in fact isolated from $P$, and there
is no way that $P$ can exfiltrate $C$'s data. $C$ can proceed using the service
as it desires.

%\subsection{Joint Computation}
%\label{sec:examples:joint-computation}

\begin{figure}
\centering
\begin{lstlisting}[escapeinside={@}{@}]
cap @\textbf{request\_service}@(cap service_rp, cap node_rp):
    membrane = create(Membrane)
    wrapped_rp = membrane.wrap(service_rp)
    while not node_rp.empty():
        wrapped_rp.send(node_rp.recv())
    result_rp = wrapped_rp.recv()
    membrane.clear()
    return result_rp
\end{lstlisting}
\caption{Application as a Service customer's code}
\label{lst:aaas-customer}
\end{figure}

\begin{figure}
\centering
\begin{lstlisting}[escapeinside={@}{@}]
cap @\textbf{receive\_service\_request}@(cap service_rp, cap node_rp):
    while not service_rp.empty():
        node = service_rp.recv()
        lease = node.reset()
        node_rp.send(node)
        node_rp.send(lease)

void @\textbf{finalize\_service}@(cap service_rp, cap result_rp):
    service_rp.send(result_rp)

void @\textbf{full\_service}@(cap service_rp):
    node_rp = create(RendezvousPoint)
    receive_service_request(service_rp, node_rp)
    # Use the values in the node_rp to set up our service and configure the
    # system...
    result_rp = ...
    finalize_service(service_rp, result_rp)
\end{lstlisting}
\caption{Application as a Service provider's code}
\label{lst:aaas-provider}
\end{figure}

