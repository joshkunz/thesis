\chapter{Securing Galaxy}
\label{sec:galaxy}

%In section~\ref{sec:examples} we
%laid out a protocol that could be applied to address such problems. To
%prove this point, we propose extending this work, to try and enhance the
%security and data privacy inside of a modern science workflow system using
%\name{}. 

Modern bioscience, especially in the area of genetics, heavily relies on the
ability to execute a series of statistical and transformational analyses over large
(tens to hundreds of gigabytes) datasets. These datasets contain many different
types of information such as genetic data, and patient medical records. To aid
researchers in performing their analyses, several ``scientific workflow'' systems
have been developed (Taverna~\cite{taverna}, Galaxy~\cite{galaxy}). Using these 
scientific workflow systems, researchers can construct ``workflows'' that consist of a series of 
``tools'' that are piped together in an operation graph. Each tool performs one
phase of the analyses the researcher wishes to perform.

%``Source'' tools retrieve
%data, and feed it into any number of intermediate tools that perform analyses
%or transform the data and further feed it into more intermediate tools until 
%the data finally reaches one or more ``sink'' tools that generate the artifacts 
%of the analysis. 

These scientific workflows systems are important to researchers for two reasons.
First, they allow them to automate complex processes (which may take hours or
days to execute) in a user-friendly graphical way. This allows them to
more easily construct complex workflows, understand why or how they work (or don't work), and
share workflows to allow for easy fully reproducible experiments. Secondly, these
workflow systems provide an \textit{infrastructure} that makes it unnecessary
for researchers to become system administrators and maintain their own data
analysis clusters. These workflow systems are usually provided to researchers
as a platform, where a researcher can make an account in a publicly provided
system, and then perform their analyses using the infrastructure provided by
the service operator. Researchers upload their datasets to this infrastructure,
and then use these datasets as inputs to the analysis workflows. The results of
these workflows can then be downloaded from the workflow system, or used as
the input to further workflows.

Though using these systems drastically lowers the data management and administration
workload for researches, little emphasis is put on data security or privacy. Datasets
uploaded by the researcher are stored on a global filesystem that is accessible by
any tool used by any researcher. Not to mention that all data are visible to the
Galaxy system itself. If a researcher wishes to use confidential information
in a scientific workflow system, their only current option is to host it themselves,
again becoming system administrators for their own infrastructure.

%Because the researcher must upload their datasets to the workflow system provider,
%the types of data that can be used on these publicly-provided workflow systems 
%is limited. The researcher must be willing to reveal their data to the
%provider to allow the workflow 
%
%These systems provide no mechanism to secure information 
%from different users of the system, or even different tools in the tool chain.
%Unfortunately, this means that the researcher must trust
%the service provider with whatever data they would like to perform the analysis
%on. This limitation means that such platforms are effectively unusable for
%most analyses that involve sensitive human genetic data, or patient medical
%records. Further, a user may want to use a proprietary service to do their data 
%analysis, that trusts neither galaxy, nor the user. 
%The process of securing such a system is non-trivial, as it involves large amounts
%of private data involved in complex analysis routines being performed by a large
%set of tools created by a number of different authors.

To address the issues of privacy and security in scientific workflow applications,
we apply \name{} to an existing scientific workflow system, Galaxy. We leverage
the ``Application as a Service'' protocol to ensure that researcher data stays
private, even while being accessed by arbitrary ``tool'' scripts. We do this
with minimal modifications to Galaxy, and no modifications to the ``tool''
scripts themselves.

A simple overview
of the Galaxy architecture is depicted in Figure~\ref{fig:galaxy-arch}.
Users interact with the Galaxy system through a web-based frontend. Through this
portal, they can create new workflows, or inspect the results of previous workflow
executions. At a high level, in Galaxy, a workflow is a graph of ``tools'' that 
are arbitrary scripts. Each tool has an XML description that describes the script
to execute, as well as any inputs, how they are to be supplied (via standard-in,
as arguments, etc.), and any outputs (files, etc.). It's worth noting that there
are three types of nodes in this graph: sources, sinks, and transform nodes. Sources
are scripts that take no inputs. These nodes are responsible for fetching the
data that will be used in the workflow. Sinks are the output of the workflow;
they are nodes with no outputs connected to other tools. What we will call
the ``transformation'' nodes are nodes that take input, and produce output.
These tools are typically responsible for the actual analysis.
Once a user has described a workflow in the Galaxy frontend, they tell the
Galaxy frontend to ``execute'' it. At this point, the Galaxy frontend forwards
the workflow to the Galaxy control node. The control node is responsible for
orchestrating the execution of a workflow. It has access to a tool database
that contains all of the scripts that can be used in workflows. For each node
in the workflow graph, the Galaxy control node selects the appropriate tool script
from the tool database, and sends it to a free worker node. Additionally, it sends
any required input data to the worker node. This worker node executes the script
and then sends the resulting data back to the Galaxy control node, who stores
it in the workflow data database. Once every node has been executed, the Galaxy
control node gives the frontend the results, so they can be displayed to the user.

In order to make this workflow secure, we assume that the tenant is on the
same cloud as the Galaxy instance (control node, and worker nodes). We leverage
the \name{} Application as a Service protocol to execute the tools in isolation,
storing the data on a node provided by the user, instead of a database managed
by the Galaxy control node. In the isolated case, the process is very similar
to the above. However, when requesting a workflow execution, the user supplies
a token (that can be looked up via the broker) to an RP for a data node controlled
by the user. Instead of directly executing the tool on the worker node, Galaxy
sends a \textit{capability} to the worker node to the user's data node. The
user can then use this node capability to execute the Application as a Service
protocol against the service provider that will be providing the tool. To show
that our system can implement the same abstraction that Galaxy does, we'll consider
the Galaxy control node itself to be the tool provider. However, the Application
as a Service protocol does not require this, meaning our system actually supports
independent and proprietary 3rd-party tools, which Galaxy does not.
Once the user's data node has execute the Application as a Service protocol against
the Galaxy control node, the control node can install the
appropriate tool script that will be executed against the researcher's data. 
We call the configured node a ``tool node'' as it is responsible
for executing a single tool. After the protocol is complete, the data node can be sure
that the tool node is isolated from Galaxy. The tool node will request the input
data it needs from the data node it is now connected too. Once the tool is done
executing, it can send the result data back to the user's data node. Once the
result data has been received, the user's data node signals the control node, which
can disable the user's access to the node by revoking the node capability it sent
at the beginning of the process. Since the control node does not posses a NodeLease
to the Node, it must be reset before it is used in another execution, and therefore
will be wiped. Once this process has been done for every node
in the workflow, the results will be on the data node, available for the user 
to access.

%Specifically,
%we propose three extensions. First, we can isolate the workflow tools from
%each other using the basic isolation properties provided by \name{}. Second,
%we can enable researchers to use public workflow infrastructure as long as they
%are co-resident on the same cloud as the workflow infrastructure. Finally, 
%we can provide a protocol that will allow untrusted third-party providers to be
%used in the analysis process.
%
%To explain how this is achieved, we must first explain how the Galaxy system
%itself works.
%The overall architecture of Galaxy is depicted in Figure.

\section{Implementation}
\label{sec:galaxy-impl}

To implement this extension, we first wrote a \name{}-aware job running system.
The workflow required by \name{} is not a standard job-control workflow, due to
the fact that the operator who ``installs'' the job and the operator who
executes the job are separate, physically partitioned entities. The \name{}-aware
job running system consists of three pieces: The data-node manager, the cluster manager,
and a runner. Jobs are sent to the cluster manager who coordinates the setup and
execution of the job. The data-node is controlled by the owner of the data the
job is executing on, i.e., the researcher in the case of Galaxy. When a job needs
to be executed, a description of the job is sent to the cluster manager. It specifies:
an identifier that can be used to lookup a rendezvous point for the data-node,
a specification of the job to run, and a list of the input and output files.
The identifier is used to bootstrap communication between the data-node and
the cluster manager. The cluster manager sends a node to the data-node that
the data-node uses to directly perform the Application as a Service protocol against
the cluster manager. Once during the phase of the Application as a Service protocol,
where the service provider has access to the resources, the cluster manager installs
the runner script, and gives it a specification of the job to run. This specification
consists of the job that was originally enqueued at the cluster manager, and
the input/output mapping that was provided simultaneously. The runner stores
this specification until it is given access to the actual data-node at the end
of the Application as a Service protocol. At that point, the runner fetches all
input data, executes the job, and uploads any output data. The actual credentials
or information required to access the data-store are supplied to the runner
after the Application as a Service protocol has been completed. The job is considered
to be finished once the runner notifies the data-node manager of job completion.
At that point, the data-node manager tells the cluster manager that that node is
no longer needed. The data-node's rights can be revoked, and the node can be
re-added to a set of available worker nodes. In the case that the data-node is malicious
and doesn't want to return the worker node, the cluster-manager can employ a timeout
and forcibly reclaim the node once the timer expires.

Secondly, we wrote a Galaxy job runner plugin that utilized this \name{}-aware job
runner to execute the required jobs. Due to the variety of grid computing systems with which
Galaxy interoperates, it has a clean separation between the job runner system
and Galaxy as a whole. Galaxy exposes a basic \texttt{queue\_job} interface that
provides the plugin with a specification of the tool that will be run, including
the set of input and output files the tool will consume or produce. The \name{} Galaxy
plugin generates the input and output mapping required by bootstraping off of Galaxy's
existing data management system. Instead of storing the actual file contents,
the plugin stores pointers to the files. It stores the RendezvousPoint identifier
for the data-node that holds the files, and the identifier for the file itself.
For the output files, it infers the RendezvousPoint identifier from the input files,
and generates random identifiers for the output files. By pregenerating the output
file identifiers, the plugin doesn't have to try and recover the location of the
output files; it already knows. To start the workflow, the researcher can use
a simple ``\name{} Import Tool'' to ensure that the plugin is aware of the input
file location. This tool is configured to just write the identifier to the
data file, so it can be used as an input for a job run by the actual job runner plugin.
To recover the output of a workflow, the researcher can look at the output data file
in Galaxy to see the key of the final output. Using that key, they can look up the
result in their data-node's data store.

\section{Limitations and Future Directions}
\label{sec:galaxy:future}

%\subsection{Implicit vs Explicit Galaxy Metadata)

While the \name{}-aware Galaxy system provides significant improvements in terms
of usability, it does have some limitations. The principal among them is that Galaxy's
interactive features can no longer be used with the \name{}-aware system. By default,
Galaxy's tools can supply postprocessing scripts that add metadata to their outputs.
The Galaxy control node can then read this metadata, and present a more interactive frontend interface
to the user. For example, using this metadata, the Galaxy system can prepopulate
fields for the user to select, or perform automatic conversions when a datatype
does not match the input datatype for the tool. This is not possible in the \name{}-aware Galaxy
because \name{} enforces a strict separation of the data node and the Galaxy control node.
Any generated metadata files are only accessible to the data node, not the
Galaxy control node. This is fundamental to the data isolation. If data from the
tool-node were allowed to pass back to the data-node, it  would be a vector for
data leaks. This problem can be solved in two ways. First, the Galaxy system itself
could stop providing interactive features that would be broken by \name{} to its
users. This is preferable to users who would rather have additional security than
additional functionality. Finally, a data-node tool for accessing and displaying the
metadata could be created. Such a tool would have access to the metadata, and
could allow the user access to its meatadata. 

%\subsection{Verifying workflow integrity.}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figs/galaxy-arch.pdf}
    \caption{Architecture of the Galaxy scientific workflow system.}
    \label{fig:galaxy-arch}
\end{figure}

