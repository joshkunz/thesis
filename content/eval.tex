\chapter{Evaluation}
\label{sec:eval}

Our evaluation focuses on the performance and scalability of \name{}'s capability
operations in the context of a simple cloud-based, multiparty
Application as a Service case study.

We first examine performance of \capnet{}'s core capability operations,
and show that they are low-cost, and that even the most expensive 
operations are fast. Second, we discuss performance of the master workflow agents
executing capability operations and building communication paths,
relative to execution time of a simple Hadoop job, to show that the total
time to create a \capnet{} network configuration is reasonable.
Third, we evaluate the scalability of the \capnet{} SDN controller by
running multiple concurrent AaaS tenant pair experiments, to show that our
controller can scale within a multitenant cloud. Finally, we evaluate the
\name{}-extended Galaxy system described in Chapter~\ref{sec:galaxy}, showing
that it adds additional security and functionality compared to the state of
the art.

\section{Application as a Service Software Case Study}
We built two master workflow agents, a realization of the ``Application as a Service'' 
collaborative \capnet{} protocol described in Chapter~\ref{sec:examples}, each of which 
is owned and run by a different OpenStack tenant. The first master workflow agent 
(the ``user WFA'') receives a list of node capabilities to VMs that were 
allocated by a user tenant and attached to the \capnet{} network. The second 
master workflow agent (the ``service WFA''), running in a different tenant, 
registers with the \capnet{} broker object to provide the ``Hadoop''
configuration service. The user WFA looks up the Hadoop service
using the broker object, and using the rendezvous point object capability it
receives, sends capabilities to its VMs to the service WFA. Once received by
the service WFA, the WFA resets these nodes to clear their capabilities, 
creates an all-pairs set of flow capabilities, giving each VM the ability to 
talk to every other (Hadoop requires that all nodes communicate), and installs 
and configures Hadoop. Finally, the service WFA
sends the grant capability associated with the Hadoop master node back across the membrane
to the user WFA\@.  The user WFA then ``clears'' the
membrane, revoking the service WFA's capabilities to the VMs it just configured. 
Finally, the user WFA runs the Hadoop \texttt{wordcount} job on an input 
dictionary file that is sized according to the number of slave nodes (so that 
each slave could process a 128\,MB file).

\subsection{Experiment Infrastructure}
We conducted our experiments on an OpenStack cluster configured with \capnet{}.
The cluster contains 26 machines: one node functions as the OpenStack ``controller''
(authentication and API services); another node as the ``network manager'' 
(runs network-wide services such as DHCP, DNS, the OpenStack metadata proxy---and
\capnet{} services such as master workflow agents); and compute nodes that host 
VMs.  Each machine is a Dell PowerEdge R430 with two 2.4 GHz 8-core E5-2630 
processors, 64~GB RAM, and one 200~GB SSD, running Ubuntu 15.10, Linux 
kernel 4.2.0-27, OpenStack ``Liberty'', and {Open~vSwitch} 2.4.0.  
Each node is connected to a 10~Gb Ethernet LAN (the \capnet{} physical data plane).
The network manager and compute nodes each have a single {Open~vSwitch} bridge
(containing the physical Ethernet device) %, each of which
controlled by the \capnet{} controller.

\subsection{Test Setup}
We ran the master workflow agents on new VMs a total of 60
times: 15 trials each with 50, 100, 150, and 200 slaves.  Each
set of 15 trials operated on an identical input file
(approximately 6.4, 12.8, 19.2, and 25.6\,GB, respectively).


\section{Capability Operation Benchmarks}

In this section, we examine the overhead of various capability protocol messages.
These operations fall into two categories: ``Soft'' operations that only affect
the state of objects on the controller, and ``Hard'' operations that can affect the
network itself
(i.e., that cause flow add or removal). Table~\ref{table:cap-op-time-ranges} shows the range
of observed execution times for Soft operations (as a group) and select
Hard operations that are important to the function of the 
Application as a Service protocol.
Most hard operations take only a few hundred microseconds. 
In the worst case, 
clearing a membrane involves deleting hundreds of flow capabilities that all
manipulate the state of the network, yet it still takes only 
hundreds of milliseconds.

The operations \texttt{create(Flow)},
\texttt{Grant.grant} are the primary vectors for flow capabilities. Since network
state is updated
when a new flow capability is received, the timings
for these two operations represent the expected cost of altering the network
connectivity of \capnet{} nodes. Extrapolating from these
operation timings, we can see that our network manipulations take only 
100-200$\mu$s on average.

The complex %other two operations appearing in Table~\ref{table:cap-op-time-ranges},
\texttt{Node.reset} and \texttt{Membrane.clear} operations
re-isolate a node and destroy a membrane, respectively.
\texttt{Membrane.clear} is \capnet{}'s most costly operation.
They are expensive because they may make many changes to
the underlying network state. For example, when a membrane is destroyed,
flow capabilities on the wrong ``side'' of the membrane will
be deleted; this is the primary motivation for membranes.
Figure~\ref{fig:time-membrane-clear} shows a CDF of the time to execute
a membrane clear in each experiment. 
The cost of \texttt{Membrane.clear} increases proportional
to the large numbers of wrapped capabilities in the larger experiments.
In our largest
experiment, \texttt{Membrane.clear} took no longer than $\approx$~600ms. Since
\texttt{Membrane.clear} is only invoked a few times in a protocol to re-isolate
exposed nodes, it is
unlikely to be a major bottleneck. 

When \texttt{Node.reset} is invoked to re-isolate a node, all flow capabilities
pointing ``to'' the node must be revoked. Depending on the number of principals
that own a given flow to a node that is being reset, this operation can become
expensive. A CDF of the execution time of \texttt{Node.reset} for each
experiment is shown in Figure~\ref{fig:time-node-reset}. The cost of
\texttt{Node.reset} is mostly invariant on the number of nodes, since
most nodes have the same number of incoming
flows when reset. Only the data up to the 99th percentile is show in the graph
for clarity of description. The maximum time taken by a \texttt{Node.reset} 
operation was 1.3ms.

\section{Workflow Agent and Hadoop Performance}

Here we analyze the ``macro'' performance of the master workflow
agents executing the AaaS protocol.  We show the overhead of groups of
the costly capability operations, compared to the time spent configuring and running
Hadoop.  (We do \emph{not} show the ``soft'' capability operations in
these tables, since they are low-cost and not
major contributors to total WFA times.)

Table~\ref{table:user-wfa} lists time spent in key phases in the user WFA, while
Table~\ref{table:service-wfa} lists time spent in the service WFA\@.

The user WFA receives capabilities to nodes in its tenant from the
controller (``recv-nodes''); it uses a 30\,s timeout to detect when the
controller has finished sending.
We create the service WFA prior to VMs;
thus, the ``membrane-recv'' operation in the service WFA is lengthy (the
time includes VM creation).
During``membr-wait-recv'', the user WFA waits for the service WFA to send
the capability back through the membrane, signaling that it
has completed Hadoop setup (``hadoop-setup''); and then clears the membrane to
revoke capabilities from the service WFA.
The user WFA loads data into HDFS and runs a Hadoop job (``hadoop-job-run'').


\section{Multiple Simultaneous AaaS Workflow Agents}

In this section, we evaluate the scalability of both capability
operations and master workflow agent performance by running multiple,
concurrent {AaaS} master workflow agent pairs.  In this example, we do not
configure or run Hadoop, so the execution of the master workflow agents
consists only of capability operations, as well as the time required to
boot the VMs.  We do this by design to force each master workflow agent pairs'
capability operations to operate nearly simultaneously,
to encourage parallelism and lock contention at
\capnet{}'s controller---to allow us to analyze
\capnet{}'s scalability.

\subsection{Test Setup}
We ran an AaaS WFA pair for each tenant, and we increased the
number of concurrent tenants.  For each of 2, 3, and 4
concurrent tenants, we ran 5 trials, each with 50 and 100
slave nodes. %---30 individual tests.
This test does not run Hadoop so we set
per-slave RAM to 2\,GB and 1 VCPU to achieve greater packing.
We do not use 150- and 200-slave tests in this
experiment for several reasons.  For instance, our tuned Neutron
configuration
produced errors on large parallel VM creates; this
prohibitively increased the test runtime.
However, the number of slaves is much less important than
the number of competing tenants, which is our focus in this test.

\subsection{Capability Operation Benchmarks}
Table~\ref{table:cap-op-time-ranges-parallel} shows the timings of two capability
operations, \texttt{Grant.grant} and \texttt{Membrane.clear}, depending on the numbers
of nodes and parallel AaaS executions (i.e., 
2 instances means 4 master workflow applications running). As shown in the table,
the execution time for a given capability operation scales similarly to the
single instance case (Table~\ref{table:cap-op-time-ranges}); there is no significant
slowdown from running multiple tenants in parallel.

\section{Evaluating Galaxy Extensions}

The extensions to the Galaxy scientific workflow system (described in Chapter~\ref{sec:galaxy})
through the addition of a \name{}-aware job running system provides a functional
example of how \name{} can be used to augment the security of existing systems with
relatively minimal overhead. To evaluate these extensions, we will provide a
security analysis, showing that the system adds significant security over the
state of the art, and a functional analysis to show that a system like Galaxy
can incorporate \name{} with minimal effort.

\subsection{Security Analysis}
To describe how the \name{} system augments the security of Galaxy, we must
first describe Galaxy's current threat model. Galaxy's threat model is a classic
full-trust model. The users must trust the Galaxy workflow system with its data.
Both the Galaxy application and the tools it exposes have full read/write access
to all data on the system. The Galaxy administrator controls the set of tools,
so it can be assumed that these tools are at least as trustworthy as the Galaxy
administrator. Instead of isolating users of the system directly, users are
expected to run isolated galaxy instances that they themselves administer.

In the \name{} augmented Galaxy workflow system, the trust between the user
and the Galaxy system is broken. We assume that Galaxy may be directly malicious,
or that the tools installed in the Galaxy instance are malicious. We also utilize
the Application as a Service protocol, and therefore rely on its security assumptions.
Namely, we assume that The \name{} model is implemented correctly as described.
For our analysis, we will be concerned with three possible actors: the user,
a tool, or the Galaxy system itself. The primary goal of the malicious tools or Galaxy
systems are to compromise user data. The user's primary goal is to unfairly use
Galaxy resources. For simplicity of description, the \name{} job runner will be considered
a component of the Galaxy system.

First, note that the Galaxy system never has access to the data itself, or
even a connection to the data that is not mediated by the user. The data node
only exposes the data to the runner node once the membrane has been revoked
and all access to the Galaxy system has been destroyed. Since the Galaxy system
must reset the node before establishing a network connection to it, all state
will be wiped, ensuring that results from previous job executions are not leaked.
Since the Galaxy system never has access to the data, it is clearly not possible
for it to compromise the user's data.

The currently executing tool does have temporary access to the data, but it isolated using the Application
as a Service protocol. The protocol guarantees that the tool cannot access the
data until all of its external access (to the Galaxy system, or to the outside world)
is cut off. Even if the tool and the Galaxy system collude, they cannot re-establish
a connection after the membrane is cleared. As long as the system for retrieving
and uploading data only allows nondestructive writes, the tool cannot tamper
with data that already exists on the data node. Additionally, the node may attempt
to store data in such a way that it can be retrieved on a later invocation. Since
the node is reset with each invocation, no state persists between invocations,
and data leaking cannot occur through this mechanism.
The tool may attempt to execute
a Denial of Service attack against the node, but that is outside of the scope
of this threat model. 

Clearly, as long as the \name{} system functions properly, we can defend against
malicious tools and malicious Galaxy system operators. Additionally, since all
resources can always be reclaimed by their owners (using a revoke and reset),
it is impossible for the user to unfairly hold Galaxy resources.

\subsection{Functional Analysis}

Converting Galaxy to use a \name{} job runner involved limited modification of the
actual Galaxy system itself, though it did disable a number of Galaxy features
that could not exist given that the data are not present. The Galaxy system
exposed a standard job-queuing interface that meshed well with a \name{} enabled
job runner. Storing \name{} aware pointers to user data, instead of the data
itself, disabled interactive features of Galaxy. For example, for certain tools,
Galaxy will present a list of options based on the input dataset. Since the
\name{} datasets were not valid, it was not able to infer that any options were
valid. This is fundamental to the data isolation. A system where the data must
be separated from the user interface cannot expose data-dependent behaviour to the user
without breaking the isolation of the system. Galaxy is still able to execute
most of its operations, and can still use these tools in a noninteractive
``workflow-based'' mode.

\begin{table}
\centering
\caption{Capability operation timings (min. to 99th perc. in $\mu$s).}
\tabletitleskip
\footnotesize
\begin{tabular}{@{}l|r|r|r|r@{}}
%%  \hline
\setlength\tabcolsep{1.5pt}
  \textbf{Operation} & \multicolumn{4}{c}{\textbf{Number Slave Nodes}} \\
   & 50 & 100 & 150 & 200 \\
  \hline
% Soft
Soft     & 0-1120 & 0-1160 & 0-1270 & 0-1090 \\
% Network
\hline
create(Flow)      & 20-650  & 50-290  & 30-290  & 40-300  \\
Grant.grant       & 120-460 & 100-450 & 100-440 & 70-460  \\
Node.reset        & 170-720 & 160-680 & 150-674 & 130-720 \\
Membrane.clear    & 52880-  & 158340- & 319140- & 505650- \\
                  &  72050  &  202030 &  364730 &  594150 \\
%Membrane.recv     & 680-1140  & 0-200   & 0-230   & 0-230   \\
\end{tabular}
\label{table:cap-op-time-ranges}
\end{table}

\begin{figure}
      \centering
      \includegraphics[width=4in,trim=0 2.85in 0 0,clip]{figs/membrane-clear.pdf}
      \caption{Membrane clear}
      \label{fig:time-membrane-clear}
\end{figure}

\begin{figure}
      \centering
      \includegraphics[width=4in,trim=0 2.85in 0 0,clip]{figs/node-reset.pdf}
      \caption{Node reset (up to 99th percentile)}
      \label{fig:time-node-reset}
\end{figure}


\begin{table}
\centering
\caption{User WFA time (sec.) in selected functional blocks, and full
  execution time (columns do not sum total).}
\tabletitleskip
\footnotesize
\begin{tabular}{@{}l|r|r|r|r@{}}
\setlength\tabcolsep{1.5pt}
  \textbf{Operation} & \multicolumn{4}{c}{\textbf{Number Slave Nodes}} \\
   & 50 & 100 & 150 & 200 \\
  \hline
  recv-nodes & 36.5012 & 42.8794 & 49.5650 & 56.0228 \\
  send-nodes & 1.1590 & 2.1302 & 3.0669 & 4.2292 \\
  membr-wait-recv & 71.8752 & 122.3947 & 265.3045 & 260.5325 \\
  membrane-clear & 0.1005 & 0.2109 & 0.3806 & 0.5902 \\
  hadoop-job-run & 151.2563 & 246.9126 & 325.0591 & 432.6685 \\
  \emph{full WFA time} & 261.7379 & 419.2941 & 654.1214 & 766.4997
\end{tabular}
\label{table:user-wfa}
\end{table}

\begin{table}
\centering
\caption{Service WFA time (sec.) in selected functional blocks, and full
  execution time (not sum total).}
\tabletitleskip
\footnotesize
\begin{tabular}{@{}l|r|r|r|r@{}}
\setlength\tabcolsep{1.5pt}
  \textbf{Operation} & \multicolumn{4}{c}{\textbf{Number Slave Nodes}} \\
   & 50 & 100 & 150 & 200 \\
  \hline
  membrane-recv & 95.0663 & 143.7468 & 192.3154 & 254.1871 \\
  recv-nodes & 8.3839 & 16.2502 & 24.0745 & 32.1975 \\
  allpairs & 8.7366 & 33.1489 & 74.0023 & 128.9374 \\
  hadoop-setup & 56.0935 & 75.4767 & 170.7995 & 104.2655 \\
  \emph{full WFA time} & 168.6481 & 268.8255 & 461.6599 & 519.5675
\end{tabular}
\label{table:service-wfa}
\end{table}

\begin{table}[!h]
\centering
\caption{Capability operation timings with parallel Application as a Service 
         (minimum to 99th percentile in $\mu$s).}
\tabletitleskip
\footnotesize
\setlength\tabcolsep{4pt}
\begin{tabular}{@{}l|r|r|r|r|r|r@{}}
  \textbf{Operation}
    & \multicolumn{2}{c|}{\textbf{2 instances}}
    & \multicolumn{2}{c|}{\textbf{3 instances}}
    & \multicolumn{2}{c}{\textbf{4 instances}} \\
   & 50 & 100 & 50 & 100 & 50 & 100 \\
  \hline
  Grant.grant    & 100-430 & 110-440 & 90-450 & 70-450  & 60-430 & 50-460 \\
  Membrane       & 38890-  & 162880- & 52090- & 141380- & 40040- & 144600- \\
  \,\,\,\,.clear   & 67920   & 206510  & 69480  & 201110  & 69020  & 209840
\end{tabular}
\label{table:cap-op-time-ranges-parallel}
\end{table}
