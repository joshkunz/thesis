\chapter{Introduction}
\label{sec:intro}

%    \item Clouds are great
%    \begin{itemize}
%        \item Commodities compute / data / networking
%        \item Allow lots of low-cost workloads to run together, owned by different
%              tenants.
%    \end{itemize}
%    \item Clouds have problems
%    \begin{itemize}
%        \item Some tenants have different incentives. They may want to steal data
%              from other tenants, or steal their resources
%        \item Clouds have to defend against this
%        \item Clouds have defended against this using strict inter-tenant isolation
%              (VMs, seperate virtual networks, authenticated APIs)
%        \item Even with this isolation, inside of a tenant clouds still give
%              VMs ambient network authority. With more processes running on a
%              cloud, more complex systems and isolation schemes are required to
%              realize users requirements.
%    \end{itemize}
%    \item Clouds Could be better
%    \begin{itemize}
%        \item Despite having lots of different data and workloads, they are
%              all separate, there is no way to hook them together.
%        \item Cloud assumes that tenants never want to share resources or
%              utilize systems or data provided by other cloud tenants
%    \end{itemize}

While cloud infrastructure has massively increased access to large-scale compute
resources, by allowing dynamic provisioning by multiple users, it has introduced
new security challenges not faced in traditional ``enterprise-like'' deployments.
In a standard enterprise datacenter environment, it can usually be assumed that
all resources contained within have some level of mutual trust, or at least have
common incentives, specifically, to achieve the goals of the datacenter owner.
If an enterprise datacenter is secured from external threats, it can be assumed
that there will be no purposeful internal threats. As such, enterprise networks
make little effort to isolate their resources internally. Public-facing 
servers with large attack surfaces may be isolated from, say, all internal
devices on the enterprise's network, but the internal devices may not be isolated
from \textit{each other}. This is due to the fact that, by default, networks
implement an \emph{Ambient Authority} security policy. Connectivity is ``on'' by
default, and must be restricted using Internet protocol (IP) routes, firewalls, or other means.
Due to this, administrators typically don't take the extra effort to isolate
internal devices from each other, instead opting to isolate internal resources
from external ones. This creates a catastrophic failure mode for the compromise
of a single internal device. Once an attacker is able to gain leverage in
the internal domain, they have free reign to attack all other subsystems.
% ideas:
%   internal resources are disparate: difficult to manage
%   internal resources might not be as well maintained, might not have
%       as many resources dedicated to management

Unfortunately, the transition to cloud infrastructure has not changed this aspect
of enterprise access control. Clouds provide their tenants with ``user'' and ``project''
abstractions that model, in the cloud, a typical enterprise domain: a set of resources
connected by an Ambient Authority network with strong isolation from the 
``outside world'' through firewalls. We argue that replicating the enterprise
access control model in a cloud is wrong on two fronts. First, it retains the
property of ambient network authority that is present in enterprise networks.
Instead, we advocate the Principle of Least Authority (PoLA), where resources
begin with no network connectivity and must be \textit{granted} the right to
communicate from an administrator. This helps mitigate the catastrophic failure
mode described above, by requiring the administrator to explicitly model the 
authority of each resource over another. Second, it limits the ability of
cloud tenants to interact with each other, by placing them in walled gardens.
By collocating many different workloads in a single datacenter, it becomes
feasible for appliance providers to directly provide their services to other
cloud tenants. This is common on clouds today~\cite{aws-mrkt-render-street,aws-mrkt-stripe,aws-mrkt-moonmail}, with some clouds even
providing explicit support for it. However, the types of services are limited
by the abstractions cloud providers supply for inter-tenant interactions. Most
such services are supplied as either Virtual Appliances as a Service (VAaaS)
where an appliance provider gives a customer access to a prepackaged Virtual Machine (VM) that the
customer can then deploy in their cloud, or what we call ``remote services'' where
the customer is expected to use an Application Programming Interface (API) provided by the service provider.
In the first (VAaaS) case, the service provider must be able to package their
services as a VM (which cannot easily be done), and trust the user with the
software contained within, which may be proprietary. In the second case,
the customer must trust the remote provider not to steal the data supplied to
their API. We argue that by carefully relaxing the isolation between tenants,
many new forms of tenant-tenant interaction, such as mutually-distrusting services,
can be realized.

%    \item Cloud Capabilities
%    \begin{itemize}
%        \item We apply an object capability model to the cloud
%        \item The right to connect to a node, or to administer it's access control
%              policy can be exchanged in a flexible and dynamic way. This allows
%              for more complex isolation schemes inside of a single tenant.
%        \item Also, capabilities can be given to other tenants, for mutual administration
%        \item We can also introduce new objects to this model that can allow a
%              tenant or multiple tenants to set up complex isolation schemes
%              including mutually distrusting applications as a service, which
%              is a major problem on current clouds.
%     \end{itemize}

To this end, we propose XNet. XNet is a cloud extension that adds an object
capability system to an existing cloud. Capabilities are an access control
mechanism that allow for flexible control over the access control policy. Resources
in the system are modeled as ``objects'' and the right to manipulate the resource
associated with an object is represented as a pointer or capability to 
one of these resource objects. The access control policy is described by the current
distribution of capabilities to users in the system.
The capabilities can be exchanged between users, allowing for flexible control over not only what
the access control policy is, but who controls the policy. In XNet, we use capabilities
to construct an additive security policy framework. There is, by default, no connectivity
between cloud resources; all connectivity must be constructed by manipulating the
policy graph, thereby enforcing the Principal of Least Authority.
Since these policy control capabilities can be exchanged, we can leverage them
to accomplish the type of cross-tennant collaboration described earlier.

\section{Thesis Statement} 

Augmenting current cloud access control systems with
capabilities enhances functionality and security of real-world 
applications, with low run-time costs.

%     \item What we did
%     \begin{itemize}
%        \item Came up with a set of objects and such that map onto cloud stuff.
%        \item We wrote an SDN controller that implements our model
%        \item We integrated it with OpenStack to run real cloud workflows
%        \item We came up with some dummy applications and tested them
%     \end{itemize}

We have developed an implementation of \name{} based on the OpenStack
cloud system. We have constructed a Software Defined Networking (SDN) controller 
that integrates with the OpenStack
networking driver interface, allowing us to create ``capability enabled'' networks
in OpenStack.
We have also created some basic example applications that illustrate the types
of collaboration possible with the \name{} framework. For example, we have a working
example of a secure collaborative hadoop instance. Finally, to show that \name{}
can be used to add security to existing real-world systems, we have extended the Galaxy~\cite{galaxy}
scientific workflow system to use \name{}. This adds significant security over
the existing system, while maintaining most of the remaining functionality.

Specifically, our contributions are as follows:
\begin{itemize}
    \item The application of an object capability model to cloud access control,
          where cloud resources (such as VMs, and network connectivity) are 
          represented by objects, and capabilities are used to control them.
    \item The construction of a ``mutual isolation'' protocol based on our 
          cloud capability model, as well as a ``Membrane'' object inspired
          by existing research on capability systems. This protocol enables
          novel tenant-to-tenant services on \name{}-enabled clouds.
    \item A prototype implementation of our cloud-specific object-capability
          that is capable of enforcing policies defined in the model on SDN-enabled
          networks.
    \item A \name{}-enabled cloud that integrates our prototype controller with
          the OpenStack cloud orchestration system.
    \item A set of \name{}-aware extensions to the Galaxy scientific workflow system
          that meaningfully extend the security of the Galaxy system, specifically, allowing
          scientists to keep their data private while using the system.
    \item Several microbenchmarks of the \name{} controller's overhead, showing it
          adds sub-millisecond overhead to most operations.
\end{itemize}
