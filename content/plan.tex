\section{Proposed Extension}
\label{sec:extensions}

We believe that the \name{} framework described in this document is useful and
applicable to many practical cloud problems. In section~\ref{sec:examples} we
laid out a protocol that could be applied to address such problems. To
prove this point, we propose extending this work, to try and enhance the
security and data privacy inside of a modern science workflow system using
\name{}. Modern bioscience, especially in the area of genetics, heavily relies on the
ability to execute a series of statistical and transformational analyses over large
(tens to hundreds of gigabytes) datasets. These datasets can contain many different
types of information such as genetic data, and patient medical records. To aid
researchers in performing these analyses, several ``scientific workflow'' systems
have been developed (Taverna~\cite{taverna}, Galaxy~\cite{galaxy}). Some of these are
open-source, and others are proprietary services. Using these scientific workflow
systems, researchers can construct ``workflows'' which consist of a series of 
``tools'' which are piped together in an operation graph. ``Source'' tools retrieve
data, and feed it into any number of intermediate tools that perform analyses
or transform the data and further feed it into more intermediate tools until 
the data finally reaches one or more ``sink'' tools that generate the artifacts 
of the analysis. 

These scientific workflows systems are important to researchers for two reasons.
First, they allow them to automate complex processes (which may take hours or
days to execute) in a user-friendly graphical way. This allows researchers to
more easily construct complex workflows, understand why or how they failed, and
share workflows to allow for fully reproducible experiments. Secondly, these
workflow systems provide an \textit{infrastructure} that makes it unnecessary
for researchers to become system administrators and maintain their own data
analysis clusters. These workflow systems are usually provided to researchers
as a platform, where a researcher can make an account in a publicly provided
system, and then perform their analyses using the infrastructure provided by
the service operator. These systems provide no mechanism to secure information 
from different users of the system, or even different tools in the tool chain.
Unfortunately, this means that the researcher must trust
the service provider with whatever data they would like to perform the analysis
on. This limitation means that such platforms are effectively unusable for
most analyses that involve sensitive human genetic data, or patient medical
records. Further, a user may want to use a proprietary service to do their data 
analysis, that trusts neither galaxy, nor the user. 
The process of securing such a system is non-trivial, as it involves large amounts
of private data involved in complex analysis routines being performed by a large
set of tools created by a number of different authors.


We argue, that by using the security and isolation primitives of \name{} we can
enhance the security of an existing scientific workflow system, Galaxy, to address
all of the previously mentioned issues. Specifically,
we propose three extensions. First, we can isolate the workflow tools from
each other using the basic isolation properties provided by \name{}. Second,
we can enable researchers to use public workflow infrastructure as long as they
are co-resident on the same cloud as the workflow infrastructure. Finally, 
we can provide a protocol that will allow untrusted third-party providers to be
used in the analysis process.

To explain how this is achieved, we must first explain how the Galaxy system
itself works.
The overall architecture of Galaxy is depicted in Figure~\ref{fig:galaxy-arch}.
Users interact with the galaxy system through a web-based frontend. Through this
portal, they can create new workflows, or inspect the results of previous workflow
executions. At a high level, in Galaxy, a workflow is a graph of ``tools'' which
are arbitrary scripts. Each tool has an XML description that describes the script
to execute, as well as any inputs, how they are to be supplied (via standard-in,
as arguments, etc.), and any outputs (files, etc.). It's worth noting that there
are three types of nodes in this graph: sources, sinks, and transform nodes. Sources
are scripts that take no inputs. These nodes are responsible for fetching the
data that will be used in the workflow. Sinks are the output of the workflow,
they are nodes with no outputs connected to other tools. What we will call
the ``transformation'' nodes, are nodes that take input, and produce output.
These tools are typically responsible for the actual analysis.
Once a user has described a workflow in the Galaxy frontend, they tell the
galaxy frontend to ``execute'' it. At this point, the galaxy frontend forwards
the workflow to the Galaxy control node. The control node is responsible for
orchestrating the execution of a workflow. It has access to a tool database
that contains all of the scripts that can be used in workflows. For each node
in the workflow graph, the Galaxy control node selects the appropriate tool script
from the tool database, and sends it to a free worker node. Additionally, it sends
any required input data to the worker node. This worker node executes the script
and then sends the resulting data back to the galaxy control node, who stores
it in the workflow data database. Once every node has been executed, the galaxy
control node gives the frontend the results, so they can be displayed to the user.

In order to make this workflow secure, we assume that the tenant is on the
same cloud as the galaxy instance (control node, and worker nodes). We leverage
the CapNet Application as a Service protocol to execute the tools in isolation,
storing the data on a node provided by the user, instead of a database managed
by the Galaxy control node. In the isolated case, the process is very similar
to the above. However, when requesting a workflow execution, the user supplies
a token (that can be looked up via the broker) to an RP for a data node controlled
by the user. Instead of directly executing the tool on the worker node, Galaxy
sends a \textit{capability} to the worker node to the user's data node. The user's
data node then executes the Application as a Service protocol against the galaxy
control node itself. During this process, the galaxy control node can install the
appropriate script. We call the configured node a ``tool node'' as it is responsible
for executing a single tool. After the protocol is complete, the data node can be sure
that the tool node is isolated from galaxy. The tool node will request the input
data it needs from the data node it is now connected too. Once the tool is done
executing, it can send the result data back to the user's data node. Once the
result data has been received, the user's data node signals the control node, who
can disable the user's access to the node by revoking the node capability it sent
at the beginning of the process. Once this process has been done for every node
in the workflow, the results will be on the data node, available for the user 
to access.

\section{Thesis Plan}
\label{sec:plan}

In this section I describe the work that I have completed (specifically highlighting
my contributions to the larger project), and the work that I plan to perform
for my thesis.

\subsection{Completed Work}
\label{sec:plan:completed}

I have made three major contributions to the current state of the work. First,
I help define and formalize the semantics of the model. For example, I am 
responsible for initially describing most of the semantics of membranes, and how
they apply to the example secure protocol described in section~\ref{sec:examples}. Further, I
concretely defined a secure protocol, showing how it could be constructed
using the exact semantics of the model. Second, I was primarily responsible
for implementing the capability controller described in section~\ref{sec:impl}, and the
client library that communicates with this controller. This required
additional refinements on the model and the definition and implementation of the
capability protocol. Additionally, I helped write the implementation of the
application as a service protocol. Finally, I assisted in the evaluation of the
system, primarily the evaluation of the capability controller. I instrumented
the controller, and performed the necessary data analysis.

\subsection{Proposed Work}
\label{sec:plan:proposed}

I propose to extend the existing work in two ways. First, I plan to extend
Galaxy with the application as a service protocol as described in~\ref{sec:extensions}.
I will additionally evaluate the performance impact of extensions to show
that they have a negligible impact on the performance of Galaxy. Below
is my expected timetable:

\begin{trivlist}
\item \textbf{05-05-2017}: CapNet integrated with galaxy, some workflow supported
      by capabilities.
\item \textbf{05-12-2017}: Evaluation of the extension complete. Specifically the
      evaluation shows that the additional capability operations add negligible
      cost to the capability system.
\item \textbf{05-12-2017}: Thesis Draft Delivered.
\item \textbf{05-26-2017}: Thesis Defense.
\end{trivlist}

%TIMETABLE.

\begin{figure}
    \begin{center}
    \includegraphics[width=\linewidth]{figs/galaxy-arch.pdf}
    \caption{Architecture of the Galaxy scientific workflow system.}
    \end{center}
    \label{fig:galaxy-arch}
\end{figure}
