\chapter{Implementation}
\label{sec:impl}

We have implemented a proof-of-concept of the \name{} capability-enabled cloud
based on the OpenStack cloud controller. The implementation consists of
three parts: A set of OpenStack extensions that allow the \name{} network
controller to operate as the network controller in OpenStack, a capability-enabled
network controller that implements the capability model, and a client library and
wire protocol that allow the capability-enabled clients to interact with the
\name{} network controller.

\section{OpenStack Integration}
\label{sec:impl:openstack}

The OpenStack integration is mainly composed of an \name{} OpenStack network
driver implemented using the ML2 network interface. It is largely based on
the OpenVSwitch driver~\cite{os.ml2} that is commonly used in OpenStack.
This is the \name{} controller's
primary method of interaction with the OpenStack cloud controller. 

As new VMs are created, the OpenStack compute controller informs our \name{} driver
that a new virtual port has been added, along with the network location of
this new virtual port. The \name{} driver then plumbs the virtual interface into
the local OpenVSwitch instance (that is shared by all virtual machines on a 
single physical node) and tells the \name{} controller about this newly created
virtual interface. This not only includes the physical (switch, port) location of
the new node, but its identifying network information as assigned by OpenStack
(its MAC and IP in our implementation) as mentioned in Chapter~\ref{sec:model}.
The \name{} controller then acts as the OpenFlow controller for this local 
OpenVSwitch, pushing flows onto and popping flows off of the switch as appropriate.

In addition to this network driver, we extend the OpenStack project API to include
a flag that toggles whether or not a project will be managed by the \name{} controller
or the standard OpenStack management scheme. If a project includes the
``capability-enabled'' flag, it is also expected to pass a python program that will
act as the ``Master Workflow-Agent'' for that project. The master workflow agent
is modeled similarly to a Node. It is run in a network namespace on the OpenStack
that has a single interface connected to a local OpenVSwitch that is controlled
by the \name{} controller. It communicates with the \name{} controller using
the same API that is used by nodes on the system. When it is created, the
\name{} OpenStack network driver informs the \name{} controller using the same
mechanism it uses for new nodes. However, it also passes a special ``master''
flag. This flag signifies to the \name{} controller that this ``node'' should
receive capabilities for any new nodes created in the same project, as well
as a capability to the special ``broker'' object that is described in
Chapter~\ref{sec:model}, which is the basis of inter-tenant collaboration.
This master workflow-agent is the bridge that connects the concepts of ``project''
and ``user'' into the capability world. Additionally, in very simple setups,
it can be used to manage all network access policy without having to orchestrate
a complex policy setup on multiple nodes, or add capability-aware code to legacy
applications.

\section{Controller}
\label{sec:impl:controller}

As mentioned, the \name{} controller is a standard OpenFlow controller that
implements the \name{} capability model. The controller written in C and based on
the OpenMUL OpenFlow application framework. It consists of three main parts:
\texttt{libcap}, \texttt{libobj}, and the wire protocol implementation. \texttt{libcap}
is a capability library that implements the most basic features of an object
capability system. It implements the concept of ``capability pointers'' contained
in ``capability spaces'' and provides mechanisms to map system pointers to capability
pointers, retrieve the system pointers from capability pointers, as well as
perform primitive operations like grant, delete, and revoke on capabilities.
\texttt{libcap} is responsible for implementing the important Capability Derivation
Tree (CDT) data structure that tracks the lineage of a capability, and enables
objects like Membranes and the \texttt{revoke} primitive to work. On top of this
framework, the \name{} controller adds \texttt{libobj}. \texttt{libobj} implements
the objects described in Chapter~\ref{sec:model}. These object's implementations
directly call the \texttt{libcap} API to realize their specifications. Additionally,
\texttt{libobj} object-specific callbacks to implement special functionality
when certain \texttt{libcap} methods are invoked like ``grant''. These callbacks
are used to add meta-information to capabilities (like their wrap state), or,
in the case of flows, realize the operations in the actual system.

A primary job of the \name{} controller is to implement the policy described
by the current distribution of flow capabilities in the network. To achieve this,
when a flow capability is first granted to a node, the (unidirectional) connectivity
is reified into an OpenFlow rule and pushed onto the first-hop switch of the
receiving node. By default, the \name{} controller implements a ``deny all'' policy,
keeping with the ``principal of least authority''; traffic is only permitted to flow
after the node receives the appropriate flow capability. Since a single node
may hold multiple capabilities to a single flow object, the \name{} controller uses
reference counting to ensure that a flow is pushed onto the network for only the
first capability to a given flow a node receives. When a node loses a flow
capability (via \texttt{delete}, \texttt{revoke}, etc.) the reference count for
the flow object is decreased. When the count reaches zero, the flow is deleted
from the first-hop switch and the node loses its access. We would like to restate
that all capability-related operations happen purely in the control plane. The
enforcement of the policy is done in the dataplane, and does not involve the
\name{} controller being ``in the loop''.

Finally, the controller implements an interface that allows for capability-enabled
nodes in the network to execute capability operations on the controller. As
described in Section~\ref{sec:impl:openstack}, OpenStack informs the controller
of the (switch, port) a node is located on. On startup, the \name{} controller
installs a rule that matches packets with a special capability-protocol ethertype,
sending such capabilities to the controller. When a node sends a packet with the
correct ethertype, the packet is sent to the controller who parses the
message, looks up the node object associated with the (switch, port) the packet
was received on, and then attempts to execute the operation. The capability operation
is performed as if it was being performed by the actual node object, so only
capabilities that are valid for that object are valid in the request. Since the
(switch, port) to node binding is unforgeable, this ensures that nodes are only
allowed to execute legal capability operations using this mechanism. That is to
say, they are only allowed to execute the methods of the objects they have capabilities
for. If the operation the node executes returns any results (for example, a capability
to a newly created object), the controller constructs a new response packet
and directly injects it onto the sender first-hop switch. This ensures that
such responses are only sent to the correct sender. However, since capabilities are
bound to the sending (switch, port), there is little an attacker could do with
such information.

\section{Wire Protocol}
\label{sec:impl:wire-protocol}

The wire-level protocol implemented by the \name{} OpenFlow controller is based
on Google protocol buffers~\cite{protobufs:url}. It is a request/response-based protocol. Each
capability request packet has an ``operation'' flag that corresponds to one
of the primitive operations described in Chapter~\ref{sec:model}, such as
\texttt{create} or \texttt{invoke}. Capabilities are exchanged as 64-bit numbers.
The protocol utilizes the ethernet frame directly and therefore does not benefit
from any of the additional features of protocols like TCP. To support
certain asynchronous messages, the protocol supports retransmission, but does
not support fragmentation and assumes that all messages can be contained in an
ethernet MTU. In practice, this is not an issue, as all of our messages are
significantly smaller than an MTU.

\section{Client Library}
\label{sec:impl:library}

To allow cloud tenants to easily interact with the capability system, we have also
implemented an API that abstracts the wire protocol into a simple object-oriented
interface that matches the model. The client library utilizes dummy objects that store the
capability for the ``real'' object internally. When the client invokes a method
on such a dummy object, the library creates a capability protocol request and
sends it to the controller, marshalling the response into the appropriate 
return object. 

In addition to these basic operations, the client library implements some helper
utilities for commonly performed routines (like receiving and then resetting 
a collection of nodes), and the ``Application as a Service'' protocol described
earlier in Chapter~\ref{sec:examples}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figs/capnet_os.pdf}
    \caption{\name{} implementation}
    \label{fig:capnet-impl}
\end{figure}
