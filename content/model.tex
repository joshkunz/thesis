\chapter{Model}
\label{sec:model}

The \name{} capability model consists of a set of objects (Table~\ref{tab:objects}) that are used to
model the cloud and the distribution of capabilities, as well as a set
of operations (Table~\ref{tab:operations}) that enable basic interaction with capabilities and the
object system. The operations are executed by principals; any capability
operations are performed ``in their context'' so to speak. A principal cannot
execute a capability operation using a capability they do not own.

\section{Basic Capability Distribution}

A principal in \name{} begins its life with no capabilities, and therefore,
cannot access any objects. Instead, it only has access to a set of basic
``operations'' (detailed in Table~\ref{tab:operations}). Two of these
operations, \texttt{create} and \texttt{rp0}, connect the new principal with the
rest of the capability system. The first of these operations, the \texttt{create} interface, 
is used to
create new objects. When a principal creates an object, they receive a capability
to the newly created object as a result of the operation.
The caller selects the type of the created object, by passing it as an argument
to the \texttt{create} operation. The second of these operations, \texttt{rp0},
is invoked with no arguments, and returns a capability to a special rendezvous
point object. Rendezvous
point objects are the main mechanism of capability exchange. They act as
channels through which capabilities can be ``passed''. They implement a \texttt{send}
and \texttt{recv} interface. The former, when given a capability, will add it
to the rendezvous point's internal queue; the latter will then pop the least
recently added capability off of this queue and return it to the caller.
Together they implement a first-in-first-out capability channel.
Principals invoke these methods, like all methods, by using a third operation:
\texttt{invoke}. Invoke takes a capability to an object and the method on
that object to invoke, as well as any arguments (such as the capability to send
for the \texttt{send} method of rendezvous points).

In \name{}, all capabilities are only owned by a single principal. As part of
this exchange, a new capability is created that references the same object
as the original capability. We say that this capability is ``derived from''
the original capability. If the derived capability is then sent and
received itself, the resulting capability will be derived from the derived
capability itself. Further, the original capability could be sent and received 
again using a rendezvous point, resulting in two derived capabilities. As such,
these capabilities form a tree of derivation. In the literature, these
are called Capability Derivation Trees (CDTs). By modeling capability exchange
as a series of derived capabilities, \name{} is able to implement additional
capability management operations.

The most basic of these capability management operations is \texttt{delete}.
When a capability is deleted, it is removed from the set of capabilities the
principal controls, and it is deleted from the CDT. Derived capabilities are
not removed. This allows a single principal to de-escalate their privileges 
without affecting the privileges of others. As an extension of this, \name{} also
provides the \texttt{revoke} operation. This operation executes \texttt{delete}
on every \textit{decendent} of capability passed to it. This operation allows
the caller to ``reclaim'' their authority over a capability from any other
principal to whom they have given it. Since it only applies to the descendants of
a given capability, sub-trees of the CDT can be revoked independently.

A final operation is provided, \texttt{mint}, which implements a basic 
capability-duplication operation. \texttt{mint} takes a capability as an argument,
and returns a capability to the same object that is a child of the originally
capability in the CDT.
It has the same effect as if the principal
was to create a new RP, and then send and recv a capability using it.

%\subsection{Fundamental Capability Operations}
%
%\name{} models a set of operations over capabilities.
%These operations are used to achieve the most basic actions in the system. A
%full listing of the operations can be found in Table~\ref{tab:operations}.
%The operations fall, largely, into two categories: those for creating new
%capabilities, and those for destroying old capabilities. Objects in
%\name{} are created using the \texttt{create} operation. This operation
%creates a new object with the given type, and returns a \textit{capability}
%to the newly created object to the caller. This is how almost all capabilities
%enter the system, via a \texttt{create} call. Once a capability is no longer
%needed, it can be destroyed with a call to \texttt{delete}. \texttt{delete}
%prevents the caller from ever using the deleted capability to invoke methods
%again. The remaining ``Extended'' operations will be covered with Rendezvous
%points in Section~\ref{sec:model:collab}.
%
%TODO: Talk about how nodes can always ``create'' flows to themselves.

\section{Modeling the Cloud}
\label{sec:model:cloud}

In order to allow users to manipulate cloud resources in the capability domain, 
\name{} models cloud resources as capability objects: \texttt{NodeOwner},
\texttt{NodeLease}, and \texttt{Flow}. The interfaces for these objects, along
with all other objects, are listed in Section~\ref{sec:model:collab}. To achieve
strong isolation and collaboration between mistrusting parties, \name{} implements
a two-tiered model of node ownership and control (see Figure~\ref{fig:node-owner}). For every node, \name{}
models a \textit{single} \texttt{NodeLease} object. A capability to this object 
gives the owner effective control over the node. This object implements methods
for every operation that can be performed in \name{} including the ability to
invoke methods on object. These methods are executed ``in the context'' of 
the modeled node. That is to say, they are executed as if they were executed by
the modeled node. When a \texttt{NodeLease} is destroyed, the associated node
is wiped clean. All capabilities owned by the node are \texttt{delete}'d and
all flows that allow sending packets \emph{to} the node are destroyed, re-isolating
the network access policy of the node. Finally, the node itself is re-booted, so
any state stored in the memory of the node is destroyed as well.

Despite the fact that a node is only ever controlled by a single party at a time,
the node may still have multiple \textit{owners}. We say a principal has ownership
if they possess a capability to the \texttt{NodeOwner} object associated with the
node. In the \name{} model, ownership
gives the owner the privilege to, at any time, acquire a new \texttt{NodeLease}
to the associated node. They do this using the \texttt{reset} method of
the \texttt{NodeOwner} object. This method destroys the old \texttt{NodeLease},
creates a new one, and returns a capability to the newly created lease to the 
caller. This reset mechanism is very useful for protocols between mutually
distrusting parties. One party can grant a capability for a \texttt{NodeOwner}
to the other untrusted party. This capability can only be used to reset the underlying
node, the first party doesn't have to worry about capabilities owned by the node
leaking from the \texttt{NodeOwner}. Additionally, once the untrusted party has
received the \texttt{NodeOwner} capability, it can use it to reset the underlying node.
This guarantees to the second party that the original owner no longer has access
to any capabilities owned by the original node. The node has been completely 
separated from its original owner.

\subsection{Interfacing with Cloud Authority}
\label{sec:model:cloud:intf}

\name{} is only concerned with the definition and enforcement of access control
policy at the level of cloud resources. Importantly, it does not model the cloud
interfaces that are used to create and destroy cloud resources. \name{} relies
on the cloud's standard resource creation system to correctly manage who can
create what resources, and who can destroy what resources. To allow a newly created
node to enter the capability-enabled world, it must know to whom the node belongs.
Due to this, \name{} must map the cloud's understanding of a resource's ``owner''
to a principal in the capability domain. To this end, \name{} introduces the
concept of a ``master workflow agent''. The master workflow agent is modeled
as a node that is associated with some cloud-level domain, like a project. When
a new node is created in a project, the master workflow agent for that project
is delivered a \texttt{NodeOwner} capability for the newly created node object.
From this capability, all ownership and control over a node is
derived. This workflow agent acts as the ``root'' of authority for all nodes
created in that project. It must act to begin \textit{any} capability exchange
between nodes in the same project.

\section{Modeling Collaboration}
\label{sec:model:collab}
A primary goal of the \name{} system was to allow the tenants on the cloud
to create inter-tenant policies that facilitated workflows that are insecure
using current cloud systems. The objects and operations above hint at some
form of inter-tenant collaboration. For example, one tenant could give another
a capability, then revoke it after some period of time, or a tenant could
reset a node to isolate it from its original owner. However, capabilities
can only be exchanged by nodes in the same tenant. There is no capability channel
that ``crosses'' multiple tenants.

\subsection{Broker} To facilitate the communication of capabilities across
tenants, \name{} gives every master workflow agent a capability to a \texttt{Broker}
object. This object exposes a simple two-method interface: \texttt{register}
and \texttt{lookup}. Using the register method, a tenant can pass a name and
a capability to be associated with that name. By communicating the name with
another tenant using an out-of-bound channel, the other tenant can use the
\texttt{lookup} interface to obtain a capability to the object that was registered
using the same name. We envision service-providers using brokers to register
``service`` rendezvous points that will allow multiple clients to initiate
service protocols and exchange capabilities, thereby ``breaking'' the full
isolation between tenants. Therefore, the broker acts as a sort of ``super-root''
capability, a capability that is shared by every tenant, and can act as a
communication channel between them. 

%TODO: Talk about how there's only one broker object, so colluding nodes can't
%use it re-isolate.

\subsection{Annotated Capabilities}
\label{sec:model:collab:annotated}

In \name{}, capabilities can be tagged with \emph{annotations}. These annotations
are added to describe the capability's lifetime and privileges. We represent
them as a set of triples of the form $\langle L, A, P \rangle$ where $L$ is the
object this capability's life is attached to, $A$ is the object that is
capable of removing this annotation, and $P$ is itself a set of allowed methods.
Typically $A = L$, meaning that the object associated with the lifetime of
the capability is often the same capability that is responsible for removing
the annotation. Restrictions on the methods that can be invoked are done
using $P$. These annotations are associative and commutative; the order they
are added and removed does not matter. The full set of enabled operations
is defined by the intersection of all $P$s in every annotation. When a capability
is returned from a method that was invoked using an annotated capability, the
result is the original returned capability with its original annotations 
unioned with the set of annotations that were on the object used to invoke
the operation.

\subsubsection{Membranes} \label{sec:model:para:membranes} Membranes are an object that is used to model the ``domains of
control'' of different tenants. Consider the case where two tenants $A$ and $B$ want to
cooperate on a data analysis problem. $A$ has some secret data it doesn't want
to give to $B$, but $A$ doesn't know how to set up the data-analytics cluster
required to efficiently perform the analysis. Using the operations and objects 
described above, $A$ has a few options. First, $A$ could reset the nodes before
giving them to $B$ (preventing $B$ from accessing any data stored on them) 
and reset them after getting them back, but that would erase any configurations that 
$B$ did to the nodes . Second, $A$ could grant $B$
access to the nodes, and then use the \texttt{revoke} operation later
to remove $B$'s access. However, this doesn't work either, as all of the capabilities
$B$ creates will ``descend'' from the original set of capabilities $A$ gave to $B$,
again, undoing any configurations $B$ has made to the policy.

Instead, we need a middle ground: a mechanism that will allow $B$ to manipulate the
policy, but allow $A$ to verify, at some point, that $B$ no longer has the
ability to manipulate the policy, without destroying the parts of $B$'s policy 
that $A$ wants to keep. Membranes provide this abstraction. Each membrane
portions capabilities into two halves: the ``inner'' half, and the ``outer''
half. By default, all capabilities are on the ``inner'' side. Capabilities can
be sent to the ``outer'' side by passing them to \texttt{membrane.wrap}. This
function returns a new capability with the annotation $\langle M, M, FULL\rangle$, 
 where $M$ is the membrane object we executed the \texttt{wrap} method on, and
$FULL$ is the full set of capability privileges (no restrictions). Due to the
annotation rules, this annotation will be added to any new capabilities that
are created from these ``outside'' capabilities. However, when a capability
with such an annotation is passed back to the \texttt{membrane.wrap} method
of the same membrane, the annotation is removed. 

Membranes can also be used to wrap rendezvous points, which will implicitly
invoke ``m.wrap'' on \texttt{send} and \texttt{recv}. These wrapped rendezvous
points act like ``channels'' between the inner and outer worlds. Anything that
passes from inner to outer gets ``wrapped'' and anything that passes from
outer to inner gets untagged. Then, at any point, the party that possesses a
capability to the membrane object can execute \texttt{membrane.clear}. This
method destroys the membrane, and therefore any capabilities that are annotated
with the membrane's annotation (since those capabilities have the same lifetime
as the membrane). Anything that has passed back through the membrane before
the membrane is cleared will be spared. This operation can be though
of as a ``selective revoke''. In fact, if capabilities never get passed
back through the membrane (never call ``membrane.wrap'' on a capability annotated
with the membrane's wrap annotation), \texttt{membrane.clear} functions
identically to the \texttt{revoke} operation.

A critical feature of membranes is that they provide a two-way guarantee. They
destroy all capabilities that have only passed one direction, regardless of 
whether that is from the inside to the outside, or the outside to the inside.
This means the membranes can be used to detect whether or not a capability $c$ is a
child of some other capability $p$. All we need to do is invoke \texttt{membrane.wrap}
on $p$, then later when we want to verify that $c$ is a child of $p$, we call
\texttt{membrane.wrap} on $c$, and then invoke \texttt{membrane.clear}. If the
capability was a child of $p$, it will still be annotated with the membrane
annotation. Invoking \texttt{membrane.wrap} will \textit{remove} the annotation,
sparing it from deletion when the membrane is cleared. If $c$ is not a child of
$p$, a wrap will be added to the resulting capability. Therefore, when the membrane
is cleared, the capability will be destroyed. This feature is critical to the
construction of mutually-distrusting collaboration protocols, as we cannot trust
the other party not to lie about the lineage of a capability.

\subsubsection{Sealers and Unsealers}
Using membranes coupled with reset, we can create nodes that are completely
isolated from all other nodes. By introducing sealer/unsealers, we can re-establish
connectivity with such isolated clusters through mutual agreement. Sealer/Unsealer
objects implement two methods: \texttt{su.seal} and \texttt{su.unseal}. When
a capability is passed to \texttt{su.seal}, a new capability to the same object
is returned, with the annotation $\langle S, S, \{\}\rangle$ where $S$ is the
Sealer/Unsealer object. This annotation prevents any methods from being executed
using this capability. A capability with such an annotation can later be
passed to the Sealer/Unsealer's \texttt{unseal} method, which will return a
new capability without the annotation, effectively ``unlocking'' the capability,
making it usable. By constructing a protocol
where sealed capabilities have to pass through multiple principals, each
principal can seal the capability with their own sealer/unsealer. Since the privileges
only ever restrict, the capability will be unusable until all the seals are removed,
and they can be removed in any order.

To enable these protocols, our model explicitly prohibits membrane wraps from
being added to capabilities to Sealer/Unsealer objects themselves, since these objects
do not themselves allow the passage of capabilities (and sealed capabilities keep
all of their additional wraps, so sealing a capability will not prevent it from
being deleted after the \texttt{membrane.clear} method is invoked on a sealed and
wrapped capability).

%The \name{} controller implements a traditional object capability system. An object
%capability system models \textit{Objects} as collections of data (much
%like objects in a programming language) that have an internal implementation
%which may expose methods that can be invoked by other objects. Objects hold
%references to each other using \textit{Capabilities}. When an object wants to
%invoke a method on another object, it uses a capability to holds to that
%object. Capabilities are unforgable, an object cannot obtain a capability without
%being explicitly given it. Capabilities can be obtained from the system
%itself when a new object is created, or passed between objects using methods. 
%
%In \name{}, every object except for \textit{NodeLifetime} has a specific, trusted
%implementation, with well-defined semantics. The \textit{NodeLifetime} object
%is used to model the actions of the actual compute resources in the cloud 
%system. As described in Section~\ref{sec:arch}, the compute resources in the
%network communicate with the \name{} using a specific API. When a node sends
%the controller a message to invoke a specific procedure exposed by the API,
%the controller executes the procedure as if it was executed by the associated
%\textit{NodeLifetime} object.
%
%\subsection{NodeLifetime Operations}
%\label{sec:model:operations}
%
%This section describes the capability model implemented by the \name{}
%network controller. It consists of a set of \textit{Objects} and 
%\textit{Operations}. Objects, like in a programming language, are collections
%of data and code. They have methods, which depend on an internal implementation
%and data which can be retrieved or manipulated by the object's implementation.
%Objects are never accessed directly, instead they are accessed through capabilities
%which act as ``pointers'' to specific object instances. One important function
%of objects in \name{} is to hold capabilities to other objects.
%
%
%Operations, on the
%other hand, are procedures exposed by the \name{} controller that allow clients
%to manipulate the objects associated with the 
%
%As described in Section~\ref{sec:arch}, the \name{} 
%
%In the \name{} model, there exist 5 basic \textit{operations} that can
%be used by principals to create capabilities, destroy capabilities, or
%invoke methods on objects. Using these basic operations and object methods
%they can control the distribution of capabilities in the system, and therefore
%the access policy, and meta-policy of a \name{} managed system. To begin,
%we will describe the fundamental operations and objects which provide the
%most basic functionality of \name{}.
%
%\subsection{Essential Objects and Operations}
%\label{sec:model:operations}
%
%Central to the operation of \name{} are three operations: \texttt{create},
%\texttt{invoke}, \texttt{delete}, and three objects: \textit{Rendezvous Point}s,
%\textit{Node}s, and \textit{Flow}s. When a new node boots
%
%There are four generic operations that are not associated with an object which
%work on capabilities directly: \texttt{create}, \texttt{delete}, 
%\texttt{revoke}, and \texttt{mint}. These operations handle the creation and
%destruction of capabilities at the most basic level.
%
%\paragraph{create(Type)} The most important of the basic operations, 
%\texttt{create} is how capabilities enter the capability system. When a 
%principal invokes \verb|create(Type)| a new object of type \textit{Type} is
%created, and a capability to the newly created object is returned. As described
%previously, this gives the caller the ability to invoke methods on the object
%or grant the capability to give others the ability to do the same.
%
%\paragraph{delete(cap)} This operation is how capabilities are removed from
%the system. When \verb|delete(cap)| is invoked, the caller's access to the
%capability \textit{cap} will be removed, preventing the caller from ever using
%that capability to invoke methods on the object the capability referenced. This
%is an important mechanism for performing privilege de-escalation. When all capabilities
%to an object have been deleted, the object is destroyed.
%
%\paragraph{revoke(cap) and mint(cap)} Revoke allows a capability owner
%to control the propagation of a capability through the system.
%
%\subsection{Objects}
%\label{sec:model:objects}
%
%\paragraph{RendezvousPoint} Allows for capabilities to be exchanged between principals.
%This is how two nodes can create connectivity between themselves. The connectivity
%graph is controlled by carefully specifying the capability graph, where the edges
%are these RendezvousPoint objects.
%
%\paragraph{Membrane} Membranes are a mechanism for doing isolation per ``side''
%of an operation. Effects are only allowed to stay if they are on the right ``side''
%of the membrane. When membranes are cleared they wipe out all capabilities
%on the wrong ``side'' of the system.
%
%\paragraph{SealerUnsealer} SealerUnsealers allow capabilities to be exchanged
%through an intermediate untrusted party. Essentially, sometimes a party
%will act as a gatekeeper, but we don't trust them to have the authority
%conferred by a capability. SealerUnsealers allow us to give that party the
%capability, but prevent them from using it. They can pass the capability onto
%the next party (that we presumably trust) who can take the useless capability,
%and using a SealerUnsealer obtain the original capability. 
%
%\paragraph{Broker} Acts as a root of the capability graph, and a naming services
%to bootstrap inter-tenant protocols.
%
%\paragraph{Node} At the core of the model, is the Node. Node objects represent
%virtual compute resources (VMs) deployed by the cloud operator into their
%cloud. blah blah
%
%\paragraph{Flow} Flows are objects that represent \textit{network} connectivity
%between nodes. A flow is a pair of the form $(node, spec)$ where ``node'' is
%the node this flow allows connectivity too, and the ``spec'' is the specification
%of the type of traffic this flow allows. Owning a capability to a flow
%object gives the owner the right to send traffic that matches the specification
%to the node the flow specifies.
%
%\paragraph{NodeLifetime} NodeLifetime objects model discrete ownership states
%of node resources through time. Node objects have a $reset()$ method that allows
%the caller to destroy the current NodeLifetime and construct a new one. A capability 
%to a NodeLifetime object for a node gives the holder the right to manipulate the
%capabilities owned by the node (through the RP or via the NodeLifetime's legacy
%``grant()'' interface) or manipulate the connectivity of the node (by creating
%new Flow objects and capabilities). Additionally, when a new NodeLifetime is
%created, all previous capabilities and flows (to and from the node) are wiped
%away. The node starts in a completely isolated position.

\subsection{Building Blocks of Collaboration}
\label{sec:model:building-blocks}
When used together, the operations and object described above can form
powerful isolation primitives or ``blocks''. These blocks can be composed into
even more powerful ``protocols'' as described in Chapter~\ref{sec:examples}.
In this section, we describe these basic building blocks.

\subsubsection{Permanent Shared Administration} This building block is the simplest.
There are two nodes $A$ and $B$ that are owned by different tenants, who want to
share administration of a third node $C$ that is owned by $A$. $A$ just grants
$B$ a capability to the \texttt{NodeLease} object for $C$, and that's it. Both
$A$ and $B$ can define the policy, forever, or at least until $A$ revokes
$B$'s capability to the \texttt{NodeLease}.

\subsubsection{Flexible Shared Administration} This building block builds on
the permanent shared administration block, by adding time-based or ``flexible'' 
shared administration. By granting a capability, and
then later revoking it, two nodes can share administration for a short-period of
time.

\subsubsection{Single-Way Isolation} Using node capabilities and the
\texttt{reset} method, a tenant can ``lend'' a node to another untrusted tenant.
Consider two tenants $A$ and $B$. If $A$ wants $B$ to perform some computation on
behalf of $A$, $A$ can send $B$ the node capabilities for some set of nodes that
$B$ can use by first calling the \texttt{reset} method on them, which will ensure
that $A$ can no longer communicate with them in a networking sense and in a
capability sense. $B$ can safely give these newly reset nodes capabilities to 
resources that are essential to the computation, but that are not sharable with 
$A$. This allows $B$ to isolate its data and computation from $A$, but it doesn't
allow $A$ to do the same to $B$. $A$ still must trust $B$ with its data, since
$B$ has an unlimited ability to manipulate the nodes $A$ gave it. Further, since
this isolation is created through the capability system, it can be ``chained''
like any other capability. $B$ can pass the node capability to another
provider $C$ who can in-turn reset the node to isolate it from both $B$ and $A$.

\subsubsection{Two-Way Isolation} To achieve two-way isolation, we use the same steps
\label{sec:model:building-blocks:two-way}
as in the single-way isolation case, with the addition of a \texttt{Membrane}
object.
By sending the node capabilities to $B$
through a membrane, $A$ can ensure that after the membrane is \texttt{clear}ed
$B$ can no longer connect to any of the nodes. Also, since $B$ reset the nodes
after receiving them from $A$, it can be sure that $A$ doesn't have any access to
the nodes that $B$ has not granted to $A$. Now, $A$ still possesses node capabilities
to the nodes, so it has the ability to reset them, but this does not grant $A$ access
to any capabilities or information that $B$ has given those nodes. It may break
the system that $B$ has set up, but won't cause any information or privileges to
``leak out''. Similarly to the previous case, this can be chained. Inside of
an isolated cluster, the nodes can perform two-way isolation again to form even
finer granularity isolation bubbles. Using this building block, we can construct
any tree-like isolation control policy, but this does not suffice for some workloads.

\subsubsection{Controlled De-Isolation of Two-Way Isolated Nodes} To enable the construction
of full graphs of isolated bubbles, we can use \texttt{SealerUnsealer} objects.
The rules about membrane annotations do not apply to \texttt{SealerUnsealer}s, so
they can persist after the membrane is cleared. When we're setting up a two-way
isolated cluster, instead of directly giving the other party access at the end
of the setup, we can give them \textit{sealed access}, by first applying the
\texttt{su.seal} method to the capability. By doing this, we ensure that connectivity
to the isolated cluster can only be established with the cooperation of the owner
of the \texttt{SealerUnsealer}. The owner can then create a second two-way isolated
cluster, and the "outer" party of the isolation can pass the sealed capability
between the clusters. This can only be accomplished through
\textit{mutual agreement} of the parties. Then, using the key that exists in
both clusters, the access can be ``unlocked'' and the sealed clusters can 
be joined by exchanging capabilities.

\begin{table}
  \small
  \centering
  \caption{A full listing of capability objects and their methods.}
  \vspace{1ex}
  \begin{tabular}{@{}l@{}}
    \textbf{NodeOwner} \\
    \quad \ttfamily NodeLease node.reset() \\ 
    \hline
    \textbf{NodeLease} \\
    \quad \ttfamily node\_lease.create(...) \\
    \quad \ttfamily node\_lease.rp0(...) \\
    \quad \ttfamily node\_lease.delete(...) \\
    \quad \ttfamily node\_lease.revoke(...) \\
    \quad \ttfamily node\_lease.mint(...) \\
    \quad \ttfamily node\_lease.invoke(...) \\
    \hline
    \textbf{Flow} \\
    \hline
    \textbf{RendezvousPoint} \\
    \quad \ttfamily void rp.send(cap \emph{c}, string \emph{msg}) \\
    \quad \ttfamily (cap, string) rp.recv(int \emph{timeout}) \\
    \hline
    \textbf{Membrane} \\
    \quad \ttfamily cap m.wrap(cap \emph{c}) \\
    \hline
    \textbf{SealerUnsealer} \\
    \quad \ttfamily cap su.seal(cap \emph{c}) \\
    \quad \ttfamily cap su.unseal(cap \emph{c}) \\
    \hline
    \textbf{Broker} \\
    \quad \ttfamily void b.register(string \emph{name}, cap \emph{c}) \\
    \quad \ttfamily b.lookup(string \emph{name}, int \emph{timeout}) \\
    \hline
  \end{tabular}
  \label{tab:objects}
\end{table}

\begin{table}
  \small
  \centering
  \caption{\name{} operations.} 
  \vspace{1ex}
  \begin{tabular}{@{}l@{}}
    \ttfamily object create(object\_type \emph{type}) \\
    \texttt{void delete(cap \emph{c})} \\
    \texttt{invoke(cap \emph{c}, method\_name \emph{m}, \emph{args...})} \\
    \ttfamily cap rp0() \\
    \texttt{cap mint(cap \emph{c}, [specification \emph{spec}])} \\
    \texttt{void revoke(cap \emph{c})} \\
  \end{tabular}
  \label{tab:operations}
\end{table}

\begin{figure}
    \begin{center}
    \includegraphics[width=2.23in]{figs/node-lease-history.pdf}
    \caption{NodeLease objects modeling the lifetimes of a NodeOwner object}
    \label{fig:node-owner}
    \end{center}
\end{figure}
